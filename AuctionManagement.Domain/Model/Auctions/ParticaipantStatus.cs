﻿namespace AuctionManagement.Domain.Model.Auctions
{
    public enum ParticaipantStatus
    {
        Active = 1,
        DeActive = 2,
        Banned = 3
    }
}