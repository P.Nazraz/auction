﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Core;

namespace AuctionManagement.Domain.Model.Auctions
{
    public interface IAuctionRepository : IRepository
    {
        Auction GetById(long id);
        void Add(Auction auction);
        long GetNextId();
        bool HasOpenAuction(long sellerId);
    }
}
