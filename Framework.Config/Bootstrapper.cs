﻿using Framework.Application;
using Framework.Core;
using Framework.Core.Events;
using Framework.EF;
using SimpleInjector;

namespace Framework.Config
{
    public static class Bootstrapper
    {
        public static void WireUp(Container container)
        {

            container.Register<ICommandBus, CommandBus>(Lifestyle.Singleton);
            container.Register<IUnitOfWork, EFUnitOfWork>(Lifestyle.Scoped);

            container.Register<IEventPublisher, EventAggregator>(Lifestyle.Scoped);
            container.Register<IEventListener, EventAggregator>(Lifestyle.Scoped);
            ServiceLocator.Set(new SimpelnjectoreServiceLocator(container));
        }
    }
}
