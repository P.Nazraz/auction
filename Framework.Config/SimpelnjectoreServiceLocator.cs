﻿using Framework.Core;
using SimpleInjector;

namespace Framework.Config
{
    public class SimpelnjectoreServiceLocator : IServiceLocator
    {
        private readonly Container _container;
        public SimpelnjectoreServiceLocator(Container container)
        {
            _container = container;
        }
        public T Resolve<T>() where T : class
        {
            return _container.GetInstance<T>();
        }
    }
}
