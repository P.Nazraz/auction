﻿using AuctionManagement.Domain.Model.Auctions;
using AuctionManagement.Persistence.EF;
using AuctionManagement.Persistence.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SimpleInjector;

namespace AuctionManagement.Config
{
    public class Bootstrapper
    { 
        public static void Wireup(Container container )
        {
            container.Register<IAuctionRepository, AuctionRepository>(Lifestyle.Transient);

            container.Register<AuctionContext>(() =>
            {
                var configuration = container.GetInstance<IConfiguration>();
                var optionBuilder = new DbContextOptionsBuilder<AuctionContext>();
                var connectionString = configuration["ConnectionString:AuctionManagement"];
                optionBuilder.UseSqlServer("connectionString");
                return new AuctionContext(optionBuilder.Options);
            });
        }
    }
}
