﻿using Framework.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Framework.EF
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        public EFUnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Begin()
        {
            throw new NotImplementedException();
        }

        public async Task Commit()
        {
            await _dbContext.SaveChangesAsync();

        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }
    }
}
