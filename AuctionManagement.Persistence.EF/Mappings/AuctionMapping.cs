﻿using AuctionManagement.Domain.Model.Auctions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionManagement.Persistence.EF.Mappings
{
    class AuctionMapping : IEntityTypeConfiguration<Auction>
    {
        public void Configure(EntityTypeBuilder<Auction> builder)
        {
            builder.ToTable("Auction").HasKey(prop => prop.Id);
            //builder.Property(prop => prop.Id).HasDefaultValueSql("NEXT VALUE FOR AuctionSequence");
            builder.Property(prop => prop.Id);
            builder.Property(prop => prop.CreateDateTime).IsRequired();
            builder.Property(prop => prop.EndDateTime).IsRequired();
            builder.Property(prop => prop.ProductId).IsRequired();
            builder.OwnsOne(prop => prop.Seller, x =>
            {
                x.Property(y => y.Id).IsRequired();
                x.Property(y => y.Status).IsRequired()
                .HasConversion(
                     v => v.ToString(),
                     v => (ParticaipantStatus)Enum.Parse(typeof(ParticaipantStatus), v));
            });
            builder.OwnsOne(prop => prop.StartingPrice, x =>
            {
                x.Property(y => y.Amount).IsRequired();
                x.Property(y => y.Currency).IsRequired();
            });

            builder.OwnsOne(prop => prop.WinningBid, x =>
            {
                x.Property(y => y.BidderId).IsRequired();
                x.Property(y => y.CreateDateTime).IsRequired();
                x.OwnsOne(y => y.OfferedAmount, z =>
                {
                    z.Property(t => t.Amount).IsRequired();
                    z.Property(t => t.Currency).IsRequired();
                });

            });

            builder.OwnsMany(prop => prop.AuctionDetails, x =>
            {
                x.Property(z => z.Name);
            });


        }
    }
}
