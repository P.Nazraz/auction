﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AuctionManagement.Persistence.EF.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "AuctionSequence");

            migrationBuilder.CreateTable(
                name: "Auction",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDateTime = table.Column<DateTime>(nullable: false),
                    EndDateTime = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<long>(nullable: false),
                    Seller_Id = table.Column<long>(nullable: true),
                    Seller_Status = table.Column<string>(nullable: true),
                    StartingPrice_Amount = table.Column<long>(nullable: true),
                    StartingPrice_Currency = table.Column<string>(nullable: true),
                    WinningBid_BidderId = table.Column<long>(nullable: true),
                    WinningBid_OfferedAmount_Amount = table.Column<long>(nullable: true),
                    WinningBid_OfferedAmount_Currency = table.Column<string>(nullable: true),
                    WinningBid_CreateDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auction", x => x.Id);
                });


            migrationBuilder.CreateTable(
                name: "AuctionDetail",
                columns: table => new
                {
                    AuctionId = table.Column<long>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuctionDetail", x => new { x.Id });
                    table.ForeignKey(
                        name: "FK_AuctionDetail_Auction_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuctionDetail");

            migrationBuilder.DropTable(
                name: "Auction");

            migrationBuilder.DropSequence(
                name: "AuctionSequence");
        }
    }
}
