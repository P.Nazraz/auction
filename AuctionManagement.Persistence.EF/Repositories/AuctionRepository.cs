﻿using AuctionManagement.Domain.Model.Auctions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace AuctionManagement.Persistence.EF.Repositories
{
    public class AuctionRepository : IAuctionRepository
    {
        private readonly AuctionContext _dbContext;
        public AuctionRepository(AuctionContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Add(Auction auction)
        {
            _dbContext.Auctions.Add(auction);
        }

        public Auction GetById(long id)
        {
            return _dbContext.Auctions.FirstOrDefault(x => x.Id == id);
        }

        public long GetNextId()
        {
            var sequence = _dbContext.SequenceIds.FromSqlRaw("SELECT NEXT VALUE FOR AuctionSequence").FirstOrDefault();
            return sequence.Id;
        }

        public bool HasOpenAuction(long sellerId)
        {
            var res = _dbContext.Auctions.Any(x => x.Seller.Id == sellerId && x.EndDateTime > DateTime.Now);
            return res;
        }
    }
}
