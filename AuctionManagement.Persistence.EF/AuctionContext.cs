﻿using AuctionManagement.Domain.Model.Auctions;
using AuctionManagement.Persistence.EF.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionManagement.Persistence.EF
{
    public class AuctionContext : DbContext
    {
        public DbSet<SequenceId> SequenceIds  { get; set; }
        public DbSet<Auction> Auctions { get; set; }
        public AuctionContext(DbContextOptions<AuctionContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasSequence<int>("AuctionSequence").StartsAt(1).IncrementsBy(1).IsCyclic(false);
            modelBuilder.ApplyConfiguration(new AuctionMapping());
            base.OnModelCreating(modelBuilder);
        }
    }
}
