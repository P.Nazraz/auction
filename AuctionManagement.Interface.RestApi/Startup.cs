using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionManagement.Persistence.EF;
using Framework.Config;
using Framework.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SimpleInjector;
using SimpleInjector.Integration.Web;

namespace AuctionManagement.Interface.RestApi
{
    public class Startup
    {
        private Container container = new Container();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            services.AddControllers();
            services.AddSimpleInjector(container, options =>
            {
                options.AddAspNetCore()
                    .AddControllerActivation()
                    .AddViewComponentActivation();
            });

            //check if by using app.UseSimpleInjector(container) on Configure method it will cross wire simpelInjector and builtin DI? yes, because
            //i can reolve IConfiguration from container inside of ServiceLocator in controller
            //ServiceLocator.Set(new SimpelnjectoreServiceLocator(container)); 

            services.AddDbContext<AuctionContext>(opts =>
            {
                opts.UseSqlServer(Configuration["ConnectionString:AuctionManagement"]);
            });


            //container.Register<AuctionContext>(() =>
            //{
            //    var optionBuilder = new DbContextOptionsBuilder<AuctionContext>();
            //    var connectionString = Configuration["ConnectionString:AuctionManagement"];
            //    optionBuilder.UseSqlServer("connectionString");
            //    return new AuctionContext(optionBuilder.Options);
            //});
            AuctionManagement.Config.Bootstrapper.Wireup(container);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            app.UseSimpleInjector(container);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
